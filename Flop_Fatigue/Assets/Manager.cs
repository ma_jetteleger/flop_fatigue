﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

    public static Manager instance;

    public float Speed;
    public float Gravity;
    public Transform Floor;

    public Vector2 MaxCoordinates { get; set; }
    public Vector2 MinCoordinates { get; set; }

    void Start()
    {
        instance = this;
        
        MaxCoordinates = new Vector2(
            Floor.position.x + (Floor.localScale.x / 2),
            Floor.position.z + (Floor.localScale.z / 2)
        );

        MinCoordinates = new Vector2(
            Floor.position.x - (Floor.localScale.x / 2),
            Floor.position.z - (Floor.localScale.z / 2)
        );
    }

}
