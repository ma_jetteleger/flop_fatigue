﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float ScaleFactor;
    public Transform[] Parts;

    private Vector3 _moveDirection;
    private CharacterController _controller;
    private int[] Scores;

    void Start()
    {
        _moveDirection = Vector3.zero;
        _controller = GetComponent<CharacterController>();

        Scores = new int[] { 1, 1, 1, 1 };
    }

    void Update()
    {
        if (_controller.isGrounded)
        {
            _moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            _moveDirection = transform.TransformDirection(_moveDirection);
            _moveDirection *= Manager.instance.Speed;
        }

        _moveDirection.y -= Manager.instance.Gravity * Time.deltaTime;

        _controller.Move(_moveDirection * Time.deltaTime);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            IncreasePart(Parts[2]);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<NPC>())
        {
            Debug.Log("Enter");
        }
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<NPC>())
        {
            Debug.Log("Exit");
        }
    }

    private void IncreasePart(Transform part)
    {
        for(int i = 0; i < 4; i++)
        {
            if(Parts[i] == part)
            {
                Scores[i]++;
            }
        }

        float[] scaleScores = new float[4];
        float totalscore = 0.0f;
        for (int i = 0; i < 4; i++)
        {
            totalscore += Scores[i];
        }

        for (int i = 0; i < 4; i++)
        {
            scaleScores[i] = Scores[i] / totalscore;
        }

        for (int i = 0; i < 4; i++)
        {
            Parts[i].localScale = new Vector3(Parts[i].localScale.x, scaleScores[i], Parts[i].localScale.z);

            float scaleFactor = 0.0f;

            switch(i)
            {
                case 0:
                    scaleFactor = Parts[i].localScale.y / 2;
                    break;
                case 1:
                    scaleFactor = Parts[i - 1].localScale.y + (Parts[i].localScale.y / 2);
                    break;
                case 2:
                    scaleFactor = Parts[i - 2].localScale.y + Parts[i - 1].localScale.y + (Parts[i].localScale.y / 2);
                    break;
                case 3:
                    scaleFactor = Parts[i - 3].localScale.y + Parts[i - 2].localScale.y + Parts[i - 1].localScale.y + (Parts[i].localScale.y / 2);
                    break;
            }

            Parts[i].localPosition = new Vector3(
                Parts[i].localPosition.x,
                scaleFactor * 2f, 
                Parts[i].localPosition.z
            );
        }
    }
}
