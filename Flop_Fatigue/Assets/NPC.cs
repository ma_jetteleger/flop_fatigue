﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

    private Vector3 _moveDirection;
    private CharacterController _controller;

    void Start()
    {
        _moveDirection = Vector3.zero;
        _controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        if (_controller.isGrounded)
        {
            _moveDirection = Vector3.zero;
            _moveDirection = transform.TransformDirection(_moveDirection);
            _moveDirection *= Manager.instance.Speed;
        }

        _moveDirection.y -= Manager.instance.Gravity * Time.deltaTime;
        _controller.Move(_moveDirection * Time.deltaTime);
    }
}
